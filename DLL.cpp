/*
	Author: 	Galen Nare
	Date:   	2020-10-13
*/

#include "DNode.hpp"
#include "DLL.hpp"
#include <iostream>
#include <stdlib.h>

using namespace std;


DLL::DLL() {  // constructor - initializes an empty list
	last = NULL;
	first = NULL;
	numSongs = 0;
}

DLL::DLL(string t, string l, int m, int s) {  // constructor, initializes a list with one new node with data x
	DNode *n = new DNode(t, l, m, s);
	first = n;
	last = n;
	numSongs=1;
}

void DLL::push(string t, string a, int m, int s) {
	DNode *n = new DNode(t, a, m, s);
	if (last == NULL) {
		first = n;
		last = n;
		numSongs = 1;
	} else {
		last->next = n;
		n->prev = last;
		last = n;
		numSongs++;
	}
}

void DLL::printList() {
	cout << "List has " << numSongs << " nodes." << endl;

	DNode* node = first;
	while (node != NULL) {
		if (node->song != NULL) {
			node->song->printSong();
		} else {
			cout << "Song is null." << endl;
		}

		if (node == last) {
			break;
		}
		node = node->next;
	}
}

int DLL::remove(string t) {
	DNode* node = first;
	int i = -1;
	while (node != NULL) {
		i++;
		if (node->song != NULL && node->song->title == t) {
			cout << "Removing: ";
			node->song->printSong();

			if (node != first && node != last) {
				node->prev->next = node->next;
				node->next->prev = node->prev;
				delete node;
				numSongs--;
				return i;
			} else if (node == first && node != last) {
				node->next->prev = NULL;
				delete node;
				numSongs--;
				return i;
			} else if (node != first && node == last) {
				pop();
				numSongs--;
				return i;
			} else {
				pop();
				numSongs--;
				return i;
			}
		}

		if (node == last) {
			break;
		}
		node = node->next;
	}

	return -1;
}

Song* DLL::pop() {
	DNode* node = last;
	if (node != NULL) {
		if (node->prev != NULL) {
			node->prev == NULL;
			last = node->prev;
		} else {
			last = NULL;
			first = NULL;
		}

		Song* song = node->song;
		node->song = NULL; // Allows us to destruct the node without deleting the song
		delete node;
		return song;
	} else {
		return NULL;
	}
}

void DLL::moveUp(string t) {
	DNode* node = first;
	while (node != NULL) {
		if (node->song != NULL && node->song->title == t) {
			DNode* nodeBefore = node->prev;
			if (nodeBefore == NULL) { // Already first in the list
				return;
			} else if (nodeBefore == first) {
				first = node;
			}

			// Swap and update references
			nodeBefore->next = node->next;
			if (node->next != NULL) node->next->prev = nodeBefore;
			node->prev = nodeBefore->prev;
			if (node->prev != NULL) node->prev->next = node;
			node->next = nodeBefore;
			nodeBefore->prev = node;
			if (nodeBefore->next == NULL) {
				last = nodeBefore;
			}

			return;
		}

		if (node == last) {
			break;
		}
		node = node->next;
	}
}

void DLL::moveDown(string t) {
	DNode* node = first;
	while (node != NULL) {
		if (node->song != NULL && node->song->title == t) {
			DNode* nodeAfter = node->next;
			if (nodeAfter == NULL) { // Already last in the list
				return;
			} else if (nodeAfter == last) {
				last = node;
			}

			// Swap and update references
			node->next = nodeAfter->next;
			nodeAfter->next = node;
			nodeAfter->prev = node->prev;
			node->prev = nodeAfter;

			if (node != last) node->next->prev = node;
			if (nodeAfter->prev == NULL) {
				first = nodeAfter;
			}
			if (nodeAfter != first) nodeAfter->prev->next = nodeAfter;

			return;
		}
		if (node == last) {
			break;
		}
		node = node->next;
	}
}

int rand_func(int i) {
	return rand() % i;
}

void DLL::makeRandom() {
	if (numSongs < 2) { // Already shuffled
		return;
	}

	vector<DNode*> nodeVec; // Dump all the items into a vector so it can do the work for us
	DNode* node = first;
	while (node != NULL) {
		nodeVec.push_back(node);
		if (node == last) {
			break;
		}
		node = node->next;
	}

	random_shuffle(nodeVec.begin(), nodeVec.end(), rand_func); // Shuffle the things

	for(int i = 0; i < numSongs; i++) { // Rebuild DLL
		DNode* prevNode = NULL;
		if (i > 0) {
			prevNode = nodeVec.begin()[i - 1];
		}
		node = nodeVec.begin()[i];

		if (i == 0) {
			first = node;
			node->prev = NULL;
		} else if (i == numSongs - 1) {
			last = node;
			node->prev = prevNode;
			prevNode->next = node;
			node->next = NULL;
		} else {
			node->prev = prevNode;
			prevNode->next = node;
		}
	}
	
}

void DLL::listDuration(int* tm, int* ts) {
	*tm = 0;
	*ts = 0;

	DNode* node = first;
	while (node != NULL) {
		if (node->song != NULL) {
			*tm += node->song->min;
			*ts += node->song->sec;
			if (*ts >= 60) {
				*tm += 1;
				*ts %= 60;
			}
		}

		if (node == last) {
			break;
		}
		node = node->next;
	}
}

DLL::~DLL() {
	DNode* node = first;
	while(node != NULL) {
		node->prev = NULL;
		delete node->song;
		DNode* node_tmp = node;
		if (node == last) {
			delete node_tmp;
			break;
		}
		node = node->next;

		delete node_tmp;
	}

	last = NULL;
	first = NULL;
	numSongs = 0;
}
