# Author: 	Galen Nare
# Date:   	2020-10-13

SRCS=$(wildcard *.cpp)
OBJS=$(SRCS:.cpp=.o )

artifact: $(OBJS)
	g++ $(OBJS) -o $@


clean:
	-rm -f *.o
	-rm -f *.out
	-rm artifact


%.: %.cpp
	g++ -c $< -o $<.o


run: artifact
	./$<