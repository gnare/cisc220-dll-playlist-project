/*
	Author: 	Galen Nare
	Date:   	2020-10-13
*/

#include "DNode.hpp"

DNode::DNode() {
    DNode::song = new Song();
}

DNode::DNode(string s, string a, int lenmin, int lensec) {
    DNode::song = new Song(s, a, lenmin, lensec);
}